using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class pokusaj : MonoBehaviour
{
    public Text title;
    public Text storyText;


    public Image medal;
    public Image door1;
    public Image door2;
    public Image door3;
    public Image monkeyImg;
    public Image tigerImg;
    public Image elephantImg;
    public Image jailImg;
    public Image rip;

    public AudioClip background;
    AudioSource audio;

    private enum States
    {
        intro, doors, firstDoor, secondDoor, thirdDoor, elephant, monkey, tiger, goodDreser, badDreser, jail, death, friend
    };

    private States currentState;
    // Start is called before the first frame update
    void Start()
    {
        currentState = States.intro;
        medal.enabled = false;
        door1.enabled = false;
        door2.enabled = false;
        door3.enabled = false;
        monkeyImg.enabled = false;
        tigerImg.enabled = false;
        elephantImg.enabled = false;
        jailImg.enabled = false;
        rip.enabled = false;
        audio = gameObject.GetComponent<AudioSource>();
        audio.clip = background;
        audio.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if(currentState == States.intro)
        {
            intro();
        }
        else if(currentState == States.doors)
        {
            doors();
        }
        else if(currentState == States.firstDoor)
        {
            firstDoor();
        }
        else if(currentState == States.secondDoor)
        {
            secondDoor();
        }
        else if(currentState == States.thirdDoor)
        {
            thirdDoor();
        }
        else if(currentState == States.elephant)
        {
            elephant();
        }
        else if(currentState == States.tiger)
        {
            tiger();
        }
        else if(currentState == States.monkey)
        {
            monkey();
        }
        else if(currentState == States.goodDreser)
        {
            goodDreser();
        }
        else if(currentState == States.badDreser)
        {
            badDreser();
        }
        else if(currentState == States.jail)
        {
            jail();
        }
        else if(currentState == States.friend)
        {
            friend();
        }
        else if(currentState == States.death)
        {
            death();
        }
    }
    void doors()
    {
        title.text = "";
        storyText.text = "You woke up in a closed room. In front of you you see three doors." +
            "\nPress F to open the first door." +
            "\nPress S to open the other door." +
            "\nPress T to open the third door.";
        door1.enabled = true;
        door2.enabled = true;
        door3.enabled = true;
        if (Input.GetKeyDown(KeyCode.F))
        {
            currentState = States.firstDoor;
            door1.enabled = false;
            door2.enabled = false;
            door3.enabled = false;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            currentState = States.secondDoor;
            door1.enabled = false;
            door2.enabled = false;
            door3.enabled = false;
        }
        else if (Input.GetKeyDown(KeyCode.T))
        {
            currentState = States.thirdDoor;
            door1.enabled = false;
            door2.enabled = false;
            door3.enabled = false;
        }
    }

    void intro()
    {
        title.text = "Welcome to Zoo Circus";
        storyText.text = "Do you want to start the game?\nPress space to start";
        if (Input.GetKeyDown(KeyCode.Space))
        {
            currentState = States.doors;
        }
    }
    void firstDoor()
    {
        storyText.text = "There is an elephant in this room." +
            "The elephant is called Ella." +
            "Ella is an elephant for the circus." +
            "\nIf you want to start training Ella for circus press T." +
            "\nIf you want to kill Ella press K.";
        monkeyImg.enabled = true;
        if (Input.GetKeyDown(KeyCode.T))
        {
            currentState = States.elephant;
            monkeyImg.enabled = false;
        }
        else if (Input.GetKeyDown(KeyCode.K))
        {
            currentState = States.jail;
            monkeyImg.enabled = false;
        }
    }
    
    void secondDoor()
    {
        storyText.text = "There is a tiger in this room." +
            "The tiger is called Tony." +
            "Tony is a tiger for the circus." +
            "\nIf you want to start training Tony for circus press T." +
            "\nIf you want to kill Tony press K.";
        tigerImg.enabled = true;
        if (Input.GetKeyDown(KeyCode.T))
        {
            currentState = States.tiger;
            tigerImg.enabled = false;
        }
        else if (Input.GetKeyDown(KeyCode.K))
        {
            currentState = States.jail;
            tigerImg.enabled = false;
        }
    }

    void thirdDoor()
    {

        storyText.text = "There is a monkey in this room." +
            "The tiger is called Mimica." +
            "Mimica is a monkey for the circus." +
            "\nIf you want to start training Mimica for circus press T." +
            "\nIf you want to kill Mimica press K.";
        elephantImg.enabled = true;
        if (Input.GetKeyDown(KeyCode.T))
        {
            currentState = States.monkey;
            elephantImg.enabled = false;
        }
        else if (Input.GetKeyDown(KeyCode.K))
        {
            currentState = States.jail;
            elephantImg.enabled = false;
        }
    }

    void elephant()
    {
        storyText.text = "Ella likes to play with the ball. " +
            "Your task is to teach Ella to juggle the ball. " +
            "Choose a way to train Ella." +
            "\nIf you want to force Ella to train press B." +
            "\nIf you want to be patient in working with Ella press G.";
        if (Input.GetKeyDown(KeyCode.B))
        {
            currentState = States.badDreser;
        }
        else if (Input.GetKeyDown(KeyCode.G))
        {
            currentState = States.goodDreser;
        }
    }

    void tiger()
    {
        storyText.text = "Tony likes to climb. Your task is to teach Tony to climb the stairs." +
            "Choose a way to train Tony." +
            "\nIf you want to force Tony to train press B." +
            "\nIf you want to be patient in working with Tony press G.";
        if (Input.GetKeyDown(KeyCode.B))
        {
            currentState = States.badDreser;
        }
        else if (Input.GetKeyDown(KeyCode.G))
        {
            currentState = States.goodDreser;
        }
    }

    void monkey()
    {
        storyText.text = "Mimica likes horses. Your task is to learn Mimica to ride a horse." +
            "Choose a way to train Mimica." +
            "\nIf you want to force Mimica to train press B." +
            "\nIf you want to be patient in working with Mimica press G.";
        if (Input.GetKeyDown(KeyCode.B))
        {
            currentState = States.badDreser;
        }
        else if (Input.GetKeyDown(KeyCode.G))
        {
            currentState = States.goodDreser;
        }
    }

    void goodDreser()
    {
        storyText.text = "You managed to train your animal, but before performing in the circus your animal is scared and doesn�t want to perform." +
            "\nIf you want your animal to perform in a circus, press Y." +
            "\nIf you don't want to squeeze N.";
        if (Input.GetKeyDown(KeyCode.Y))
        {
            currentState = States.death;
        }
        else if (Input.GetKeyDown(KeyCode.N))
        {
            currentState = States.friend;
        }
    }

    void badDreser()
    {
        storyText.text = "I'm sorry, you were a bad coach and you were kicked out of the circus." +
            "\nIf you want to start playing from the beginning, press Y." +
                    "\nIf you want to get out of the game press N";
        if (Input.GetKeyDown(KeyCode.Y))
        {
            currentState = States.doors;
        }
        else if (Input.GetKeyDown(KeyCode.N))
        {
            Exit();
        }
    }

    void jail()
    {
        storyText.text = "You ended up in jail. Killing animals is illegal." +
                    "\nIf you want to start playing from the beginning, press Y." +
                    "\nIf you want to get out of the game press N";
        jailImg.enabled = true;
        if (Input.GetKeyDown(KeyCode.Y))
        {
            currentState = States.doors;
            jailImg.enabled = false;
        }
        else if (Input.GetKeyDown(KeyCode.N))
        {
            jailImg.enabled = false;
            Exit();
        }
    }

    void death()
    {
        storyText.text = "Your animal couldn't stand your training so she killed you." +
                    "\nIf you want to start playing from the beginning, press Y." +
                    "\nIf you want to get out of the game press N";
        rip.enabled = true;
        if (Input.GetKeyDown(KeyCode.Y))
        {
            currentState = States.doors;
            rip.enabled = false;
        }
        else if (Input.GetKeyDown(KeyCode.N))
        {
            rip.enabled = false;
            Exit();
        }
    }

    void friend()
    {
        storyText.text = "Congratulations." +
            "\nYou were a very good trainer for whom it is more important " +
            "\nto be friends with your animal than to make money on it." +
                    "\nIf you want to start playing from the beginning, press Y." +
                    "\nIf you want to get out of the game press N";
        medal.enabled = true;
        if (Input.GetKeyDown(KeyCode.Y))
        {
            currentState = States.doors;
            medal.enabled = false;
        }
        else if (Input.GetKeyDown(KeyCode.N))
        {
            medal.enabled = false;
            Exit();
        }
    }

    public void Exit()
    {
        Application.Quit();
    }

}
